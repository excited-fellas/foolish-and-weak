const markdownItAnchor = require("markdown-it-anchor");

module.exports = function(eleventyConfig) {
	eleventyConfig.addLayoutAlias('base', 'layouts/base.njk');
	eleventyConfig.addLayoutAlias('post', 'layouts/post.njk');

	eleventyConfig.addPassthroughCopy("src/public/css/main.css");
	eleventyConfig.addPassthroughCopy("src/public/js/main.js");
	eleventyConfig.addPassthroughCopy("src/public/js/search.js");
	eleventyConfig.addPassthroughCopy("src/public/favicon.ico");

	/**
	 * DRAFTS FEATURE START
	 * copied directly from: https://www.11ty.dev/docs/quicktips/draft-posts/
	 * */ 
	// When `permalink` is false, the file is not written to disk
	eleventyConfig.addGlobalData("eleventyComputed.permalink", function() {
		return (data) => {
			// Always skip during non-watch/serve builds
			if(data.draft && !process.env.BUILD_DRAFTS) {
				return false;
			}

			return data.permalink;
		}
	});
  // When `eleventyExcludeFromCollections` is true, the file is not included in any collections
	eleventyConfig.addGlobalData("eleventyComputed.eleventyExcludeFromCollections", function() {
		return (data) => {
			// Always exclude from non-watch/serve builds
			if(data.draft && !process.env.BUILD_DRAFTS) {
				return true;
			}

			return data.eleventyExcludeFromCollections;
		}
	});
	eleventyConfig.addGlobalData("categoriesList", function(){
		return (data)=>{
			console.log("pretext", data);
			return 1;
		}
	});

	eleventyConfig.on("eleventy.before", ({runMode}) => {
		// Set the environment variable
		if(runMode === "serve" || runMode === "watch") {
			process.env.BUILD_DRAFTS = true;
		}
	});
	/* DRAFTS FEATURE END */

	/* ANCHORS NEXT TO MD HEADINGS */
	eleventyConfig.amendLibrary("md", mdLib => {
		mdLib.use(markdownItAnchor, {
			permalink: markdownItAnchor.permalink.ariaHidden({
				placement: "after",
				class: "header-anchor",
				symbol: "#",
				ariaHidden: false,
			}),
			level: [1,2],
			slugify: eleventyConfig.getFilter("slugify")
		});
	});

	return {
		dir: {
			input: "src",
			output: "public"
		}
	}
};
