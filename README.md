# README

I guess this file has to be read backwards? idk. I'm just leaving notes for my future self here. Top is the latest, bottom is the oldest.

## Local testing

```sh
npm install
npm run start
# visit localhost:8080 in your browser
```

#### Title search mechanics are different on Home page and Advanced Search page
Basically, home page searching just tests against the title, and then **hides** everything that **doesn't match**.

However, on Advanced Search page instead of hiding elements, it is just another step of filtering before the "renderResults" function.

## Vanilla JS mess instead of a standard JS Library?
Long story short, I didn't want to import a whole library just for one searching/filtering component.
Yeah this vanilla approach requires more mental maintenance when putting in a feature, but it also doesn't need any more features than what we initially decided with the team. If some sort of grand feature pile-up will happen in the "Issues" tab, we might have to transition to a library, for now, I'll be chugging along with the vanilla JS approach.

Also, if you can completely refactor the Vanilla JS to be more readable or objectively better, don't hesitate to shoot a PR, I'm always willing to learn.

## the idea
I can render JSON files using a template. Akin to the following example: [file link](https://github.com/11ty/eleventy-base-blog/blob/main/content/feed/json.njk);
So the plan remains to just write content, and a single JSON template will then render out the JSON file, which will then be used in the home page that will have filters in it.

## initial idea
I want to use 11ty as the MD compiler, and the general framework of adding pages. Eventually when things are organized, use the "eleventy.after" script to get all the data from post headers (each game is a post), and then compile it into a .json file, which will then be used on the main page to be rendered.
After the rendered .json file, and the main filter page being done, I could set up a `filter_setup.js` file that would be in control of what filters get rendered in the index.html page. That way this project will be able to adapt to other people's projects.

