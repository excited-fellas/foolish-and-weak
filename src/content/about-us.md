---
layout: "base"
title: "About Us"
permalink: "about-us"
---
{{ '<div class="container">' }}

# What is this?
{{ metadata.description }}
# Who made this?
[Excited Fellas](https://excitedfellas.com) collective are the creators, maintainers, and curators of this website.

{{ '</div>' }}