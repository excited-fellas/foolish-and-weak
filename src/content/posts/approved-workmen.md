---
draft: false
layout: "layouts/post"
title: "Approved Workmen"
categories: [ "study", "truth",]
cost: "Why careful study of the Word is commanded in the Bible"
---
## Resources
[Slides](https://docs.google.com/presentation/d/1rMe1ZYhghkOxUFilzBDxvLb5OwExchP1PNVbrVFpIJ4/edit?usp=sharing)
## Weight of Glory
Our last camp was titled “Weight of Glory”, and even though the phrase appears only once in the Bible, it is a very broad topic. We barely scratched the surface at camp. 

I would like to address and repeat some things I said at camp, and scratch the surface a little more :) I'll speak on one of the ways the realization of “The Weight of Glory” pushes us to act. 

The phrase comes from **2 Corinthians 4:17**:  
*For momentary, light affliction is producing for us an eternal **weight of glory** far beyond all comparison,*

So what is the Weight of Glory that this verse is speaking about?

Weight simply means "importance", or "value". And Glory, refers to glorification - us becoming like Him when he appears (1 John 3:2).
So simply said, "Weight of Glory" means "Value of Glorification" - the value of being with Christ and being made like Christ.

We stand on the shoulders of giants, - meaning, we teach what has been passed on to us by people much smarter than us, who, often through great sacrifices, preserved, translated, and exposed the great truth found in the Bible. C. S. Lewis defined the glory we are to get in this way:

**Weight of Glory, p.36**:  
The glory we are to seek and pursue is "*approval*” or "*appreciation*" by God.
A bit further:
*"Nothing can eliminate from the parable the divine accolade, "Well done, thou good and faithful servant.""*

## God's Approval
Bible does talk about ways of getting approval/ appreciation/ acceptance from God. I won’t focus on all of them today, but I think these are important to at least mention today.

* Faith - Most important part of pleasing God/ getting his approval is to do whatever you do with faith. 
Hebrews 11:6:  
*"And without faith it is impossible to **please** Him, for he who comes to God must believe that He is and that He is a rewarder of those who seek Him."*  
Its most important because if you do all the other things that would please God, but do them without faith - God won’t be pleased, for “without faith it is impossible to please Him”


* Obedience - 1 Samuel 15:22:  
*"Samuel said, 'Has the Lord as much **delight** in burnt offerings and sacrifices as in obeying the voice of the Lord? Behold, **to obey** is better than sacrifice, and to heed than the fat of rams.'"*

* Righteousness and Justice - Proverbs 21:3:  
*"To do righteousness and justice is desired by the Lord more than sacrifice."*

* Life as a sacrifice - Romans 12:1:  
*"Therefore I urge you, brethren, by the mercies of God, to present your bodies a living and holy sacrifice, **acceptable** to God, which is your spiritual service of worship. And do not be conformed to this world, but be transformed by the renewing of your mind, so that you may prove what the will of God is, that which is good and acceptable and perfect."*

## Handling of the Word of God
But when it talks about **BEING APPROVED**, most of the time it refers to handling the Gospel or the Word of God:

1 Thessalonians 2:2-4:  
*[2b] we had the boldness in our God to speak to you the gospel of God amid much opposition. For our exhortation does not come from error or impurity or by way of deceit; but just as we have been **approved** by God to be entrusted with the gospel, so we speak, not as **pleasing** men, but **God** who examines our hearts.*

Paul, Silvanus, and Timothy are described as approved by God to be entrusted with the gospel, and we get more details on what “approved by God to be entrusted with the gospel” means in 

2 Timothy 2:15:  
*Be diligent* [KJV says *“study”*] *to present yourself **approved** to God as a workman who does not need to be ashamed, accurately handling the word of truth.*

So, I don’t think it's a stretch to say that studying, being diligent, putting effort into knowing the Word of God, and being careful in handling it is an important part of being approved by God, or having our lives approved by God. 

This will be the main topic of my message today - the importance of knowing what you believe.

2 Timothy 3:14-16  
*You, however, continue in the things you have learned and become convinced of, knowing from whom you have learned them,  and that from childhood you have known the sacred writings which are able to give you the **wisdom that leads to salvation** through faith which is in Christ Jesus.  All Scripture is inspired by God and profitable for teaching, for reproof, for correction, for training in righteousness;*

1 Timothy 4:13,16  
[13] *Until I come, give attention to the public reading of Scripture, to exhortation and teaching…*
[16] *Pay close attention to yourself and to your **teaching**; persevere in these things, for as you do this you will **ensure salvation** both for yourself and for those who hear you.*

Right teaching and right preaching are an important part of ensuring salvation. They are not a secondary job of the church, they are not “optional” for the church. 

## Bible as Source of Truth and Reason:
Lets start by getting a little context - Nebuchadnezzar gets proud of his own wisdom in establishing his kingdom, and God removes reason and kingdom from Nebuchadnezzar. Daniel 4:33:  
*Immediately the word concerning Nebuchadnezzar was fulfilled; and he was driven away from mankind and began eating grass like cattle, and his body was drenched with the dew of heaven until his hair had grown like eagles’ feathers and his nails like birds’ claws.*

Daniel 4:34-36(a):  
*But at the end of that period, I, Nebuchadnezzar, <u>raised my eyes toward heaven</u> and **my reason returned to me**, and I blessed the Most High and praised and honored Him who lives forever;*

If we read on we’ll see what is meant by “raised my eyes towards heaven” - it means acknowledging God as the one who establishes kings and kingdoms:

*<u>For His dominion is an everlasting dominion,*  
*And His kingdom endures from generation to generation.*  
*“All the inhabitants of the earth are accounted as nothing,*  
*But He does according to His will in the host of heaven*  
*And among the inhabitants of earth;*  
*And no one can ward off His hand*  
*Or say to Him, ‘What have You done?’</u>*  
*At that time **my reason returned to me**…*  

Other verses that say the same:

* Proverbs 2:6:  
*"For the LORD gives wisdom; From His mouth come knowledge and understanding."*  
* Proverbs 3:5-6:  
*"Trust in the LORD with all your heart and do not lean on your own understanding. In all your ways acknowledge Him, and He will make your paths straight."*

You guys know my motto, “Foolish and Weak”. It’s a reminder to me not to rely on my own understanding, but trusting the Scripture, even when it seems puzzling or unfair to me. It is a reminder to rely on His Word, rather than my own understanding which can’t lead me to Christ or produce faith. There are still things I can’t make sense of, but I trust that it is not because the Bible is flawed, but because my understanding and interpretation of the Bible is flawed. Nevertheless, I think its important to emphasize that - 

## Truth is not Subjective:
2 Peter 1:20-21:  
*But know this first of all, that no prophecy of Scripture is a matter of one’s own interpretation, for no prophecy was ever made by an act of human will, but men moved by the Holy Spirit spoke from God.*

We have examples of false interpretations of the Scriptures in the Scriptures:
1) Satan tempted Jesus with the Scriptures (Matt 4:5-7)  
*“Then the devil took Him into the holy city and had Him stand on the pinnacle of the temple, and said to Him, "If You are the Son of God, throw Yourself down; for it is written, 'He will command His angels concerning You'; and 'On their hands they will bear You up, so that You will not strike Your foot against a stone.'" Jesus said to him, "On the other hand, it is written, 'You shall not put the Lord your God to the test.'"”*

2) Pharisees Misinterpreting the law (Matthew 15:1-11)  
*Then some Pharisees and scribes came to Jesus from Jerusalem and said, "Why do Your disciples break the tradition of the elders? For they do not wash their hands when they eat bread." And He answered and said to them, "Why do you yourselves transgress the commandment of God for the sake of your tradition? For God said, 'Honor your father and mother,' and, 'He who speaks evil of father or mother is to be put to death.' But you say, 'Whoever says to his father or mother, "Whatever I have that would help you has been given to God," he is not to honor his father or his mother.' And by this you invalidated the word of God for the sake of your tradition. You hypocrites, rightly did Isaiah prophesy of you: 'This people honors Me with their lips, but their heart is far away from Me. But in vain do they worship Me, teaching as doctrines the precepts of men.'" After Jesus called the crowd to Him, He said to them, "Hear and understand. It is not what enters into the mouth that defiles the man, but what proceeds out of the mouth, this defiles the man."*

So no, Bible doesn’t mean whatever you want it to mean. There is a right and a wrong way to interpret the Scriptures, and I don’t claim to be able to interpret the Bible flawlessly. But based on these examples, to interpret the scriptures faithfully we are to 
1) Believe the Scriptures - The Devil didn’t have saving faith, he didn’t fully believe and accept the Scriptures. Believing some scriptures and discarding the others is a sure way to get a wrong interpretation.
2) Placing Truth before Tradition - interpreting Scripture based on tradition rather than based on other Scripture. Tradition doesn’t have to be some historic, or heritage belief in your family/ country, it also means “custom”, or “your own ideas”. In our individualistic society, it is easy to have our presuppositions, which are not found in the Bible, influence our interpretation of the Bible. 
3) Look to God - as seen from the example of Nebuchanezar - the truth is only found when God is recognized as the source of truth, and like in Nebuchanezar’s case, we look and pray to Him to give us reasoning heart and mind. 

## Truth is not Optional
As spoken earlier, faithful preaching and holding to the truth are not optional in the church, they are essential, since they play important role in ensuring salvation. In fact, Paul writes in 
1 Corinthians 11:19:  
*For there must also be factions among you, so that those who are <u>approved</u> may become <u>evident</u> among you.*

ESV words it this way -
1 Corinthians 11:19 (ESV):  
*for there must be factions among you in order that those who are <u>genuine</u> among you may be <u>recognized</u>.*

There will naturally be those who understand certain topics better than others, who care about certain topics more than others, who are more genuine in their faith than others. For some Christianity is just a religion of vaguely being nice to each other, for others, it is all about bringing glory to God and taking all passages in the Scripture seriously. It is natural, and even essential that there are “factions” - what some find as “love” and “kindness” others find as disgusting and repulsive. It is not possible for those factions to be in complete unity. It’s not like we disagree on something unimportant, like what Yogurt flavor is best. After all,  
*“We believe that the cause of unity in the church is best served, not by finding the lowest common denominator of doctrine, around which all can gather, but by elevating the value of truth, stating the doctrinal parameters of church or school or mission or ministry, seeking the unity that comes from the truth, and then demonstrating to the world how Christians can love each other across boundaries rather than by removing boundaries. In this way, the importance of truth is served by the existence of doctrinal borders, and unity is served by the way we love others across those borders.”*  
-DG Affirmation of Faith, Section 15.3

### Truth is not just for Teachers and Preachers

If you are not a Preacher or a Teacher - you still need to know and grow in Truth. "I am not a preacher, I'm a worshiper" is not a valid excuse - John 4:23-24:  
*"But an hour is coming, and now is, when the <u>true worshipers will worship</u> the Father <u>in</u> spirit and <u>truth</u>; for such people the Father seeks to be His worshipers. God is spirit, and those who worship Him must worship in spirit and truth."*

And even if you wouldn't classify yourself as a worshiper, Truth is necessary to be adequate and equipped for everything you do -
2 Timothy 3:16-17:  
*"All Scripture is inspired by God and profitable for teaching, for reproof, for correction, for training in righteousness; so that the man of God may be adequate, <u>equipped for every good work.</u>"*

## Closing 
* Study of Scripture is necessary, because the **truth** found in it **leads to, and ensures salvation** (2 Timothy 3:15, 1 Timothy 4:16)
* Study of Scripture is necessary, because the truth found in it **produces action** (James 2:14)
* **Truth is essential for the church, worship, and every good work** (1 Timothy 4:16, John 4:24, 2 Timothy 3:17)
* **Truth** in the Scriptures **is Objective**, not subjective
* To **see the Truth** in Scriptures **1) Believe the Scriptures, 2) Put Truth before Tradition, 3) Look to God**

I'd like to close with some examples of believers that went through trials or suffered for their doctrinal differences (that we now find true) at the hands of other Christians. 
### Augustine of Hippo
- **Years of Life**: 354–430 AD
- **Belief for which they suffered**: Doctrine of Original Sin and the necessity of divine grace
- **Form of Suffering**: Intellectual and theological conflicts with Pelagians; not a martyr but faced significant opposition and controversy

### Athanasius of Alexandria
- **Years of Life**: 296–373 AD
- **Belief for which they suffered**: Nicene Creed and the divinity of Christ
- **Form of Suffering**: Multiple exiles (five times) due to opposition from Arian factions and political conflicts within the Church

### William Tyndale
- **Years of Life**: c. 1494–1536
- **Belief for which they suffered**: Translation of the Bible into English
- **Form of Suffering**: Arrest, imprisonment, and execution by strangulation and burning at the stake

### Jan Hus
- **Years of Life**: c. 1372–1415
- **Belief for which they suffered**: Authority of Scripture, critique of Church practices (indulgences, etc)
- **Form of Suffering**: Imprisonment, trial for heresy, and burning at the stake

### John Bunyan
- **Years of Life**: 1628–1688
- **Belief for which they suffered**: Preaching without a license, nonconformist Christian beliefs
- **Form of Suffering**: Imprisonment for 12 years, separation from family, and harsh prison conditions 

All this is not to say that we need to pick fights and suffer as a result, but rather to know and understand what we believe, and take it seriously. Not flippantly. 

There are Primary Issues (Heresies), Secondary Issues, tertiary issues, and quaternary issues. 
I think its fair to distinguish between them in this way:
- Primary Issues are the ones we die for
- Secondary Issues are the ones we live and fight for
- Tertiary and Quaternary issues are not to prevent us from joining hands and working together 

What issues land in which category is a large argument on its own. Here's my opinion :)  
Primary - the differences between Christians, Mormons, and Jehovah’s Witnesses  
Secondary - Denominations, the differences between Pentecostals and Catholics or Lutherans, for example   
Tertiary issues - worship style, complementarian/egalitarian, etc.  
Quaternary issues - head coverings, homeschooling, etc.  


### Uncategorized thoughts :)
Hebrews 5:12-6:2  
*"For though by this time you ought to be teachers, you have need again for someone to teach you the elementary principles of the oracles of God, and you have come to need milk and not solid food. For everyone who partakes only of milk is not accustomed to the word of righteousness, for he is an infant. But solid food is for the mature, who because of practice have their senses trained to discern good and evil. Therefore leaving the elementary teaching about the Christ, let us press on to maturity, not laying again a foundation of repentance from dead works and of faith toward God, of instruction about washings and laying on of hands, and the resurrection of the dead and eternal judgment."*

1 Peter 3:15  
*but sanctify Christ as Lord in your hearts, always being ready to make a defense to everyone who asks you to give an account for the hope that is in you, yet with gentleness and reverence;*

Romans 14:17-18  
[17] *for the kingdom of God is not eating and drinking, but righteousness and peace and joy in the Holy Spirit. [18] For he who in this way serves Christ is acceptable to God and approved by men.* 

Weight of Glory, p.26  
*“Indeed, if we consider the unblushing promises of reward and the staggering nature of the rewards promised in the Gospels, it would seem that Our Lord finds our desires not too strong, but too weak. We are half-hearted creatures, fooling about with drink and sex and ambition when infinite joy is offered us, like an ignorant child who wants to go on making mud pies in a slum because he cannot imagine what is meant by the offer of a holiday at the sea. We are far too easily pleased.”*

Infinite joy is offered us - the joy of being with Christ, and being like Christ. We'll do our best to help you make that joy your ultimate goal, and highest desire, and you do your part, listen to the sermons, and dwell on these things during camp.

*"There is kind of happiness and wonder that makes you serious."* - C S Lewis, "The Last Battle"

Faith that doesn't bring about action is the definition of dead faith. If you believe that something is wrong and do nothing about it - what kind of faith is it?

You can be the best teacher, but if you dont have the saving faith - you wont please God. Demons know the scripture better than we do, but they are definitely not pleasing to God. 
