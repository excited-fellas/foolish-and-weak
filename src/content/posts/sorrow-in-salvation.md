---
draft: false
layout: "layouts/post"
title: "Sorrow in Salvation"
categories: [ "salvation", "emotions",]
cost: "Why we experience grief, and its place in our salvation."
---

## **Personal Testimony**
    
My personal faith did begin as just a cognitive acceptance of Jesus and His sacrifice. And I thought that I was a sigma-chad for logically, and stoically coming to believe the gospel, but today I will try to explain the role and importance of emotions in our Christian life. 

## **Emotions as sign of true salvation**
There are 3 “feelings” or “emotions” described as a fruit of the Holy Spirit just in Galatians 5:22 - love, joy, and peace. 
Other texts also highlight that TRUE religion does include emotions, emotions such as:
fear, hope, love, hatred, desire, joy, sorrow, gratitude, compassion, and zeal ([Religious Affections, p.9-11](http://www.jonathan-edwards.org/ReligiousAffections.pdf)) . 
Fear of the Lord, hope in God and in the promises of his word, love for God and people, hatred of sin, desire (or longing) for God, holy joy, religious sorrow, gratitude to God, mercy, and zeal for the Lord. 

Each of these deserves a sermon series, but today I will focus just on the “sorrow” part. 
So let's look into the matter of “when” and “why” are we to be sorrowful, according to the Bible.

## **Sorrow with Others**

First, straight forward answer is to “feel sorrowful with others”, and it can be found in 1 Corinthians 12:26, Galatians 6:2, and Romans 12:15.  
*"Rejoice with those who rejoice, and weep with those who weep”*

So, be compassionate - easy and straightforward in theory, but hard and complicated in execution. 

Along with grieving with others, we see examples of Paul grieving FOR others. Specifically, for others living in sin, separated from Christ - Phil 3:18-19, 2 Corinthians 12:21, and Romans 9:2-3.  
*"I have great sorrow and unceasing grief in my heart. For I could wish that I myself were accursed, separated from Christ for the sake of my brethren, my kinsmen according to the flesh,"*

(We also know Moses and Jeremiah mourned for the sins of their people)

## **Sorrow is caused by our rebellion**

This leads us to the next part, “why” is there sorrow and mourning - 
The answer to this question can be found in Genesis 3:16-19:  
*“To the woman He said, "I will greatly multiply Your <u>pain</u> in childbirth, In <u>pain</u> you will bring forth children; Yet your desire will be for your husband, And he will rule over you."  Then to Adam He said, "Because you have listened to the voice of your wife, and have eaten from the tree about which I commanded you, saying, 'You shall not eat from it'; <u>Cursed is the ground</u> because of you; In <u>toil</u> you will eat of it All the days of your life. "Both <u>thorns and thistles it shall grow for you</u>; And you will eat the plants of the field; By the <u>sweat of your face</u> You will eat bread, Till you return to the ground, Because from it you were taken; For you are dust, And <u>to dust you shall return.</u>"*

This passage gives us an explanation of why we have pain, why we struggle, and why we die - because we rebelled against God. The definition of sin is “rebellion against God”. We suffer bacause we rebell against God. This Rebellion grieves the Holy Spirit (Is 63:10, Ps 78:40), and should grieve us as well. 

Isaiah 63:10:   
*“But they rebelled And <u>grieved His Holy Spirit</u>; Therefore He turned Himself to become their enemy, He fought against them."*

And James 4:8-9:  
*“Draw near to God and He will draw near to you. Cleanse your hands, you sinners; and purify your hearts, you double-minded. <u>Be miserable and mourn and weep</u>; let your laughter be turned into mourning and your joy to gloom.”*

This leads to the main and final point - mourning and its role in salvation. In my opinion, Psalm 51 is the best example of repentance in the Bible. David has a proper response to the weight and seriousness of sin - he realizes what he’s done (v3-4), he mourns, and he asks to not be cast away from His presence and separated from His Holy Spirit (v11). 

Psalm 38:17-18:  
*“For I am ready to fall, And my sorrow is continually before me. For I confess my iniquity; I am full of anxiety because of my sin.”*

And we’re not to be mournful just because David is mournful, throughout the Bible we see that this is the right response to realizing your sin:

Luke 7:37-38:  
*“And there was a woman in the city who was a sinner; and when she learned that He was reclining at the table in the Pharisee’s house, she brought an alabaster vial of perfume, and standing behind Him at His feet, weeping, she began to wet His feet with her tears, and kept wiping them with the hair of her head, and kissing His feet and anointing them with the perfume.”*

And we know from verses 47-48 that she has been forgiven.

This mourning over sin also appears in “The Sermon on the Mount”, in Matthew 5:3-4:  
*"Blessed are the poor in spirit, for theirs is the kingdom of heaven. Blessed are those who mourn, for they shall be comforted."*

Again, we see that those who realize and mourn over their Spiritual depravity (1st step) receive the Kingdom Heaven.

I can’t imagine David not mourning and crying while writing Psalm 51.


The weight of sin.

Emotions are healthy and necessary when they correspond to the weight of the matter at hand.
God deserves to be feared (revered), hoped in, loved, desired. His love elicits joy and gratitude. Our sins deserve to be hated and to be sorrowed over. 

A broken heart is a pleasing sacrifice to God (Ps. 51:17)
