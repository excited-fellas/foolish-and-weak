---
draft: false
layout: "layouts/post"
title: "Christian Identity"
categories: [ "2 Peter 9-10", "Expository",]
cost: "Our identity in Christ"
---
## Resources
[Slides](https://docs.google.com/presentation/d/1xdloS-JCn_aT9n_xc5qC7UNoXVxQ53fi/edit?usp=sharing&ouid=112979199267276643418&rtpof=true&sd=true)   
[Christian Identity and Christian Destiny, John Piper](https://www.desiringgod.org/messages/christian-identity-and-christian-destiny#introduction-the-wonder-of-being-human)
## Intro

Today I would really like for us to try to understand just 2 verses from 1st Peter, which would greatly help us understand our own Identity - identity in Christ. 
Nowadays, many people struggle with their identity. People don’t really know who they really are, they struggle with things like sexuality. People often ask questions like “what am I here for?” “how can I make the difference?” People often try to define themselves by their accomplishments, and social or professional positions, or even silly things like followers on Instagram or their meme collection. Sometimes people define themselves as “group’s clown”, or similar things. But that’s not what the Bible says YOU ARE. 
## Main Scripture
### 1 Peter 2:9-10:  
*[9] But you are a <span style="background-color: red;"><u>chosen</u></span> race, a <span style="background-color: teal;"><u>royal priesthood</u></span>, a <span style="background-color: yellow;color: black;"><u>holy nation</u></span>, a people for <span style="background-color: yellow;color: black;"><u>God’s own possession</u></span>, so that you may <span style="background-color: teal;">proclaim the excellencies</span> of Him who has <span style="background-color: red;"><u>called</u></span> you out of darkness into His marvelous light; [10] for you once were not a people, but now you are the <span style="background-color: yellow;color: black;"><u>people of God</u></span>; you had not received mercy, but now you have <span style="background-color: red;"><u>received mercy </u></span>[been <span style="background-color: red;"><u>"mercied"]</u></span>.*

<span style="background-color: red;">God’s action on you.</span> <span style="background-color: yellow;color: black;">Resulting relationship of God with you.</span> <span style="background-color: teal;">Your Appointment in this world.</span>

God has <span style="background-color: red;">___ </span> you. Now, our relationship with Him is - we are <span style="background-color: yellow;color: black;">___ </span>. Our appointment is to <span style="background-color: teal;">___ </span>.

### So, Our Identity is:  
**Based on the actions of God ON you** - God <u>chose</u> you, <u>called</u> you (out of darkness into His marvelous light), and <u>“mercied”</u> you.  
**Resulting relation of God WITH you** - We are his HOLY people, or sacred, dedicated, belonging to. We are God’s own possession, God’s people.  
**Destiny that God appointed FOR you** for His glory - a royal priesthood - declaring of His excellencies among the peoples. 

Hopefully, this gives a good general idea of our Christian identity, but I’ll try to make it a bit clearer by defining what each of these terms mean.
## Chosen and Called
### Chosen
**Chosen - for what and how? What does it mean to be called?**  
2 Thessalonians 2:13-14:  
*[13] But we should always give thanks to God for you, brethren <u>beloved</u> by the Lord, because God has **<u>chosen</u>** you from the beginning <u>**for salvation**</u> through sanctification by the Spirit and Faith in truth[14] It was for this He <u>called</u> you <u>through our gospel</u>, that you may gain the glory of our Lord Jesus Christ.* 

Chosen for Salvation. Salvation is realized by means of Sanctification. Sanctification occurs by Spirit's work in us, and our Faith in truth. 

### Called 
God chose us for salvation, and made it effectual by faith in truth, which is the Gospel. For the reason of teaching us the truth we are to believe, He called us through the proclamation of the Gospel. This is the means by which He brings people into the experience of salvation.

What exactly are we called to?  
1 Corinthians 1:9:  
*God is faithful, through whom you were **called into fellowship with His Son**, Jesus Christ our Lord.*

So we’re called into fellowship with Jesus Christ. But unlike earthly relationships, where we choose our companions based on character or actions, God’s choice is rooted in His love for us (2 Thessalonians 2:13) and is determined by His purpose and grace:: 

2 Timothy 1:9:  
*who has **saved us and called us** with a holy calling, not according to our works, but **according to His purpose and grace** which was granted us in Christ Jesus from all eternity,*

Called - how? Through  the proclamation of the Gospel.  
Called - where, or to what? Into fellowship with His Son, Jesus Christ our Lord.  
Called - why? Because of His love, according to His purpose and grace, which He granted us in Christ Jesus before we could think or do anything to deserve it.
## “Mercied”, or Received Mercy:
When we receive good even though we deserve bad - its called “mercy”. God gave us mercy by taking on our sin and giving us Life, when what we deserve is death. 

Here's what Bible says about this profound truth:  
Titus 3:5:  
*He **saved** us, not on the basis of deeds which we have done in righteousness, but **according to His mercy**, by the washing of regeneration and renewing by the Holy Spirit,* 

1 Peter 1:3:  
*Blessed be the God and Father of our Lord Jesus Christ, who **according to His great mercy has caused us to be born again** to a living hope through the resurrection of Jesus Christ from the dead,*

This act of calling us and making us alive together with Christ is a demonstration of His mercy. It’s not based on our deeds or character (Titus 3:5), but on His great mercy, which is manifested through the resurrection of Jesus Christ (1 Peter 1:3) and brought to us by the renewing work of the Holy Spirit (Titus 3:5).
## Resulting Relationship
1\) Holy Nation, 2) God’s own Possession, 3) People of God:

1) Holy People - 1 Peter 1:15:  
*but like the Holy One who called you, **be holy** yourselves also in all your behavior;*

We are to pursue being like Christ - Holy, in our behavior and everything else. We are to be dedicated and belong to Christ.

2) God’s own Possession - 1 Corinthians 6:19-20:  
*Or do you not know that your body is a temple of the Holy Spirit who is in you, whom you have from God, and that **you are not your own**? For **you have been bought with a price**: therefore glorify God in your body.*

You are not your own anymore, you have been crucified with Christ, and it is no longer you who live, but Christ who lives in you. (Gal 2:20)

3) People of God - Titus 2:14:
*who gave Himself for us to redeem us from every lawless deed, and to **purify for Himself a people for His own possession**, zealous for good deeds.*

He has given Himself to redeem and purify for Himself His people. He continues to purify us through the work of the Holy Spirit. (1 Pet 1:2) 

## Our Appointment
So, our identity is based on what God has done for us, our relationship to/ with Him, and the Destiny God has appointed for you, which is:
1) To be his Royal Priesthood - 1 Peter 2:5:  
*you also, as living stones, are being built up as a spiritual house for a holy priesthood, to offer up spiritual sacrifices acceptable to God through Jesus Christ.*

We are to offer spiritual sacrificies, that are acceptable to God. These sacrifices can only be acceptable through Jesus Christ, not by our own strength…  

2) To proclaim His excellencies, though our words, our lives, our relationships, our work, our attitudes - 1 Peter 4:11:  
*Whoever speaks, is to do so as one who is speaking the utterances of God; whoever serves is to do so as one who is serving by the strength which God supplies; so that in all things God may be glorified through Jesus Christ, to whom belongs the glory and dominion forever and ever. Amen.*

To glorify God through everything we do. 
If you act the excellencies of God, people will hear the excellencies of God better. 

## Conclusion
Our Identity should be based on:
- What God has done for you 
- Your Relationship to God
- Your Purpose for God

### Practical takeaways:
- Our identity is to live to display His identity as glorious. 
- If you act the excellencies of God, people will hear the proclaimed excellencies of God better.
- We are servants of God, not our circumstances, our desires, or others’ expectations.

