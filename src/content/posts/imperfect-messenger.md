---
draft: false
layout: "layouts/post"
title: "Jonah - Imperfect Messenger of a Perfect Message"
categories: [ "Reliance on God", "Exegesis",]
cost: "Gospel in the book of Jonah"
---
## Resources
* [Slides](https://docs.google.com/presentation/d/1-hAPc6Zpw8YRaagQrMx2MNtyRZXWprQsOpqYXfxvgmA/edit?usp=sharing)  
* [John Piper on Jonah 2](https://www.desiringgod.org/light-and-truth/gods-voice-in-the-minor-prophets/divine-compassion-and-human-resistance) 

## Intro

Aight fellas, last time we read through 1 whole chapter, Lamentations 3, and today I’d like to read through a whole book of the Bible. But don’t be too scared, the whole book of Jonah is shorter than Lamentations 3.  

I only went to Sunday school until the age of 6 or 7, and I don’t remember the book of Jonah being explained, but if you do - I hope I can help you see it in a new light. “New light” not in the sense that it wasn’t there and I’m adding to it, but try to shed a light on the lesson that God teaches us through that book.   

## The whole book of Jonah
So, let’s read the whole book so we have all the context -
Jonah 1:  
<i>The word of the LORD came to Jonah the son of Amittai saying,*
"Arise, go to Nineveh the great city and cry against it, for their wickedness has come up before Me."
But Jonah rose up to flee to Tarshish from the presence of the LORD. So he went down to Joppa, found a ship which was going to Tarshish, paid the fare and went down into it to go with them to Tarshish from the presence of the LORD.
The LORD hurled a great wind on the sea and there was a great storm on the sea so that the ship was about to break up.
Then the sailors became afraid and every man cried to his god, and they threw the cargo which was in the ship into the sea to lighten it for them. But Jonah had gone below into the hold of the ship, lain down and fallen sound asleep.
So the captain approached him and said, "How is it that you are sleeping? Get up, call on your god. Perhaps your god will be concerned about us so that we will not perish."
Each man said to his mate, "Come, let us cast lots so we may learn on whose account this calamity has struck us." So they cast lots and the lot fell on Jonah.
Then they said to him, "Tell us, now! On whose account has this calamity struck us? What is your occupation? And where do you come from? What is your country? From what people are you?"
He said to them, "I am a Hebrew, and I fear the LORD God of heaven who made the sea and the dry land."
Then the men became extremely frightened and they said to him, "How could you do this?" For the men knew that he was fleeing from the presence of the LORD, because he had told them.
So they said to him, "What should we do to you that the sea may become calm for us?"—for the sea was becoming increasingly stormy.
He said to them, "Pick me up and throw me into the sea. Then the sea will become calm for you, for I know that on account of me this great storm has come upon you."
However, the men rowed desperately to return to land but they could not, for the sea was becoming even stormier against them.
Then they called on the LORD and said, "We earnestly pray, O LORD, do not let us perish on account of this man's life and do not put innocent blood on us; for You, O LORD, have done as You have pleased."
So they picked up Jonah, threw him into the sea, and the sea stopped its raging.
Then the men feared the LORD greatly, and they offered a sacrifice to the LORD and made vows.
And the LORD appointed a great fish to swallow Jonah, and Jonah was in the stomach of the fish three days and three nights. </i>

Jonah 2:  
<i>Then Jonah prayed to the LORD his God from the stomach of the fish,
and he said, "I called out of my distress to the LORD, And He answered me. I cried for help from the depth of Sheol; You heard my voice.
"For You had cast me into the deep, Into the heart of the seas, And the current engulfed me. All Your breakers and billows passed over me.
"So I said, 'I have been expelled from Your sight. Nevertheless I will look again toward Your holy temple.'
"Water encompassed me to the point of death. The great deep engulfed me, Weeds were wrapped around my head.
"I descended to the roots of the mountains. The earth with its bars was around me forever, But You have brought up my life from the pit, O LORD my God.
"While I was fainting away, I remembered the LORD, And my prayer came to You, Into Your holy temple.
"Those who regard vain idols Forsake their faithfulness,
But I will sacrifice to You With the voice of thanksgiving. That which I have vowed I will pay. Salvation is from the LORD."
Then the LORD commanded the fish, and it vomited Jonah up onto the dry land.</i>

Jonah 3:  
<i>Now the word of the LORD came to Jonah the second time, saying,
"Arise, go to Nineveh the great city and proclaim to it the proclamation which I am going to tell you."
So Jonah arose and went to Nineveh according to the word of the LORD. Now Nineveh was an exceedingly great city, a three days' walk.
Then Jonah began to go through the city one day's walk; and he cried out and said, "Yet forty days and Nineveh will be overthrown."
Then the people of Nineveh believed in God; and they called a fast and put on sackcloth from the greatest to the least of them.
When the word reached the king of Nineveh, he arose from his throne, laid aside his robe from him, covered himself with sackcloth and sat on the ashes.
He issued a proclamation and it said, "In Nineveh by the decree of the king and his nobles: Do not let man, beast, herd, or flock taste a thing. Do not let them eat or drink water.
"But both man and beast must be covered with sackcloth; and let men call on God earnestly that each may turn from his wicked way and from the violence which is in his hands.
"Who knows, God may turn and relent and withdraw His burning anger so that we will not perish."
When God saw their deeds, that they turned from their wicked way, then God relented concerning the calamity which He had declared He would bring upon them. And He did not do it. </i>  

Jonah 4:  
<i>But it greatly displeased Jonah and he became angry.
He prayed to the LORD and said, "Please LORD, was not this what I said while I was still in my own country? Therefore in order to forestall this I fled to Tarshish, for I knew that You are a gracious and compassionate God, slow to anger and abundant in lovingkindness, and one who relents concerning calamity.
"Therefore now, O LORD, please take my life from me, for death is better to me than life."
The LORD said, "Do you have good reason to be angry?"
Then Jonah went out from the city and sat east of it. There he made a shelter for himself and sat under it in the shade until he could see what would happen in the city.
So the LORD God appointed a plant and it grew up over Jonah to be a shade over his head to deliver him from his discomfort. And Jonah was extremely happy about the plant.
But God appointed a worm when dawn came the next day and it attacked the plant and it withered.
When the sun came up God appointed a scorching east wind, and the sun beat down on Jonah's head so that he became faint and begged with all his soul to die, saying, "Death is better to me than life."
Then God said to Jonah, "Do you have good reason to be angry about the plant?" And he said, "I have good reason to be angry, even to death."
Then the LORD said, "You had compassion on the plant for which you did not work and which you did not cause to grow, which came up overnight and perished overnight.
"Should I not have compassion on Nineveh, the great city in which there are more than 120,000 persons who do not know the difference between their right and left hand, as well as many animals?" </i>

## God's tangible lesson for Jonah
Few details I want to point out - Jonah 1:2-3(a):  
*“"Arise, go to Nineveh the great city and cry against it, for their wickedness has come up before Me."*  
*But Jonah rose up to <u>flee to Tarshish from the presence of the LORD</u>.”*  
**Jonah disobeyed God.**  

Jonah 1:12(b):  
*“...for I know that <u>on account of me</u> this great storm has come upon you."*  
**Jonah realizes that he has messed up.**  

Jonah 1:12(a):
“He said to them, "<u>Pick me up and throw me into the sea</u>. Then the sea will become calm for you…”  
**Jonah knows he deserves punishment for his sin.**

Jonah 2:4:  
"So I said, 'I have been expelled from Your sight. Nevertheless <u>I will look again toward Your holy temple.</u>'”  
**Jonah still looks to God for his deliverance.**

Jonah 2:6:  
"I descended to the roots of the mountains. The earth with its bars was around me forever, But <u>You have brought up my life from the pit, O LORD my God</u>.”  
**Jonah acknowledges deliverance from God from the punishment that he deserved.**

## Identical Treatment of Ninevah
Little did Jonah know, this experience that he went through was the **exact message that God meant Jonah to preach to Ninevah**, and teach us today. A message of realization of ones sins, understanding the punishment for sin, relying on God for deliverance, and that God is merciful. 

Jonah preached what he didn’t understand, he was saved, delivered, then preached deliverance to Ninevah, but expected God to NOT be merciful to Ninevah like God was to Him, to Jonah.
And for all we know, people of Ninevah were more receptive of God’s lesson, of God’s message than Jonah was. 

Jonah 1:2:  
*"Arise, go to Nineveh the great city and cry against it, for their wickedness has come up before Me."*  
**Nineveh disobeyed God by their wickedness.**

Jonah 3:5:  
*“Then the people of Nineveh believed in God; and they called a fast and put on sackcloth from the greatest to the least of them.”*  
**People realized and mourned for their sins.**

Jonah 3:9:  
*"Who knows, God may turn and relent and withdraw His burning anger so that we will not perish."*  
**They understand that their sin deserves to be punished.**  

Jonah 3:8(b):  
*“and let men call on God earnestly that each may turn from his wicked way and from the violence which is in his/(their) hands.”*  
**Ninevites looked to God for their deliverance.**

Jonah 3:10(b):  
“God relented concerning the calamity which He had declared He would bring upon them”
God relented from the calamity which he would bring upon them.  
**Same way God relented from the calamity which God brought upon Jonah**  
(Jonah 2:3 - *For <u>You</u> had cast me into the deep, Into the heart of the seas*)

## Accepting the message Jonah didn't understand
So Ninevites seemed to have received a message that Jonah himself didn’t understand. **Jonah didn’t realize that the message he was to proclaim to Ninevites was the lesson he was just taught by God.** The problem of Jonah here is that he didn’t understand God’s justice. He thought he was not a sinner like Ninevites were. He, maybe subconsciously, thought that he deserved God’s mercy more than Ninevites did - 
Jonah 4:1-2:  
*“But it greatly displeased Jonah and he became angry.*
*He prayed to the LORD and said, "Please LORD, was not this what I said while I was still in my own country? Therefore in order to forestall this I fled to Tarshish, for I knew that You are a gracious and compassionate God, slow to anger and abundant in lovingkindness, and one who relents concerning calamity.”*

**But Jonah was just like those Ninevites, he rebelled against God. He was no more deserving of God’s mercy than the Ninevites were.**  

This is the lesson that God reiterates to him in Chapter 4:  
Jonah is in great discomfort because of heat, just like he was in a great discomfort in the sea. 
Jonah tries to deliver himself from this discomfort by building himself a shelter by **his own efforts, but it's insufficient** - 
Jonah 4:5:  
*Then Jonah went out from the city and sat east of it. There <u>he made a shelter for himself</u> and sat under it in the shade until he could see what would happen in the city.*  

We’ll see why it's insufficient in 2 verses, but meanwhile, because it was insufficient, **God, by his mercy, delivers Jonah**, and gives him shade and comfort by growing a plant - 
Jonah 4:6:  
*“So the LORD <u>God appointed a plant</u> and it grew up over Jonah to be a shade over his head <u>to deliver him from his discomfort</u>. And Jonah was extremely happy about the plant.”*  

So God delivered Jonah, by His own mercy, not because Jonah deserved it. 
“Our righteousness is like filthy rags”, or in this case, like a self-made shelter in the blistering heat of Ninevah (modern-day Iraq), and “scorching east wind” - 
Jonah 4:7-8:  
*“But God appointed a worm when dawn came the next day and it attacked the plant and it withered.*
*When the sun came up God appointed a scorching east wind, and the sun beat down on Jonah's head so that he became faint and begged with all his soul to die, saying, "Death is better to me than life."”*

So Jonah Complained that God caused him discomfort by appointing a worm to eat the plant by which God, in his mercy, delivered Jonah from his discomfort at first. 
And the book ends with the word of God about His mercy and Compassion - 
Jonah 4:10-11:  
*Then the LORD said, "You had compassion on the plant <u>for which you did not work and which you did not cause to grow</u>, which came up overnight and perished overnight.*
*"Should I not have compassion on Nineveh, the great city in which there are more than 120,000 persons who do not know the difference between their right and left hand, as well as many animals?"*

## Gospel in Jonah
I think this can be interpreted as follows:  

*You are sorrowful about the deliverance for which you did not work for or cause to grow. You are sad that I removed my mercy from you when it came to heat. Should I not have mercy on those who, just like you with your shelter, can’t deliver themselves by their own power, those “who do not know the difference between their right and left hand”?* 



I don’t know about you fellas, but I find it crazy how the gospel message is so clear throughout the Old Testament. **The reason Jonah was delivered was God’s mercy towards him. The reason Ninevah was delivered was God’s mercy towards it. Jonah’s own attempt to deliver himself from the heat was pathetic at best. He needed God’s mercy, and so did the Ninevites.** 

We don’t know if Jonah understood the lesson God was teaching Him, but we know that the message of relying on God for deliverance from our calamities, often caused by our disobedience - WAS heard by the Ninevites, and that message and their reliance on God’s mercy delivered them from destruction. 

## Imperfect Messengers of a Perfect Message

And I would like to end with a short explanation of my motto - “Foolish, Weak, and Totally Depraved”. It is based on 

1 Corinthians 1:27-29:  
*"But God has chosen the foolish things of the world to shame the wise, and God has chosen the weak things of the world to shame the things which are strong, and the base things of the world and the despised God has chosen, the things that are not, so that He may nullify the things that are, so that no man may boast before God."*  
This passage emphasizes that God often chooses unlikely and imperfect people to fulfill His purposes

Romans 8:7-8:  
*"because the mind set on the flesh is hostile toward God; for it does not subject itself to the law of God, for it is not even able to do so, and those who are in the flesh cannot please God."*

I, Oles, am an imperfect messenger of God’s perfect Message. God’s Good News were not delivered to me via a perfect representation of The Gospel. I will never be able to deliver to you a perfect representation of The Gospel, that makes total sense, because we can’t explain the infinite love of God with our finite words. Moreover, I, like Jonah, may not even FULLY understand the message God is using me for. But, through His great mercy, He delivers us from destruction. And our efforts (alone) cannot deliver us from destruction, rather, we are to fully rely on Him and His mercy to deliver us. 

Preachers can be of different kinds, some preach faithfully, some not. Some preach from pure motives, and others - in pretense. 

Philippians 1:15-18:  
*"Some, to be sure, are preaching Christ even from envy and strife, but some also from good will; the latter do it out of love, knowing that I am appointed for the defense of the gospel; the former proclaim Christ out of selfish ambition rather than from pure motives, thinking to cause me distress in my imprisonment. What then? Only that in every way, whether in pretense or in truth, Christ is proclaimed; and in this I rejoice."*  
But God delivers us DESPITE the flawed messengers. When God works through me, he works not because I am good in any way, but DESPITE my foolishnes, DESPITE my weakness, and DESPITE my total depravity. 

The same way, God delivers you not because of your efforts, but DESPITE your resistance (like in the case of Jonah). All we have to do is to rely on Him, and rest in Him. 
Through a dude who didn't understand what he was preaching about, a city of 120,000 people was saved.

Psalm 62:5-8:  
*"My soul, wait in silence for God only, For my hope is from Him. He only is my rock and my salvation, My stronghold; I shall not be shaken. On God my salvation and my glory rest; The rock of my strength, my refuge is in God. Trust in Him at all times, O people; Pour out your heart before Him; God is a refuge for us."*
