---
draft: false
layout: "layouts/post"
title: "Old Covenant and its Promises"
categories: [ "Covenantalism", "Dispensationalism",]
cost: "Looking into the difference between of interpreting the Old Testament"
---
## Resources
[Slides](https://docs.google.com/presentation/d/1GMvDXAvvIRk5Zt6w055r7cVfua27NZgZvMP_2tYR6Ng/edit?usp=sharing)
## **Intro** 
Jeremiah 31:31-40. 

When I ran into this text in my yearly Bible plan and thought about what it means I realized how different the interpretation can be, based on the framework of interpretation you follow. I really liked the idea of interpreting Bible very literally, but then - to what degree do these Old Testament promises apply to me? I caught myself interpreting each promise based on what was convenient - Spiritual promises like “I will put My law within them, and on their heart I will write it; and I will be their God, and they shall be My people” (Jer 31:33b) I understood as the promises that apply to me, and land-related promises like “city will be built for the Lord from the Tower of Hananel to the Corner Gate” (Jer 31:38) only applied to ethnic Israel. But why? What in the text allows, or even prompts us to differentiate between the recipients of the promises?

## **Questions** 
To understand this passage we need to answer few questions.

First two questions we can answer:
1) Who was the promise given to? Mentioning of Israel, Judah, and theirs father specifically suggests the promise was made to Israel Specifically.
2) God will put His law in Israel’s hearts, Israel will be “God’s People”, His people will all know Him, and He will forgive His People’s iniquities, and their sins will be remembered no more.

The other two are up to interpretation:

3) Who does the promise apply to? To Israel - but who is “Israel”?
4) What do land-related promises mean? City that will be built - literally or figuratively? Who are the inhabitants? 


### Options 
Our answer to the first question, **“Who does the promise apply to?”** should, reasonably, be one of these options:


*Just the national Israel  - (Classical Dispensationalism)
*National Israel in the Church - (Covenantalism)
*Church, excluding national Israel - (Supersessionism) (barely reasonable, IMO)

Similarly, our options for **“What do land-related promises mean?”** are also reasonably limited to:

* Promises will be fulfilled for and in national Israel (in millennium)  - (Classical Dispensationalism)

* Promises were fully fulfilled in Christ - (Covenantalism and views in-between)

* Promises will be fulfilled in New Creation (New Heaven & New Earth) -  (Covenantalism and views in-between)

Your answers will depend on which framework you follow, even if you didn’t know that you followed a framework, or the names of these frameworks. Most people will find themselves somewhere in-between based on their general impressions about the Bible. Nonetheless, I think its important to know why we would lean/ be on one side or another. 

## **Definitions and Distinctions** 

### Definitions
**Framework** - A structure for supporting or enclosing something else, especially a skeletal support used as the basis for something being constructed. [[1]](https://www.wordnik.com/words/framework) 
| Framework | Dispensationalism | Covenantalism |
|---|---|---|
| **Root Word** | **Dispense**: To give or deal out, especially in parts or portions: synonym: distribute [[2]](https://www.wordnik.com/words/dispense)  | **Covenant**: A binding agreement [[3]](https://www.wordnik.com/words/covenant)|
| **Core Belief** | History is divided into multiple ages called "**dispensations**" in which God interacts with his chosen people in different ways [[4]](https://archive.org/details/backgroundstodis0000bass/page/18/mode/2up) [[5]](https://www.gotquestions.org/dispensationalism.html) | After Adam’s failure to keep the Covenant of Works, God instituted a **Covenant of Grace** [[6]](https://www.gotquestions.org/covenant-theology.html) |
| **“Partitioning” of the Bible** | Each pattern follows a pattern of: 1) a responsibility, 2) a failure, 3) a judgment, and 4) grace to move on. [[5]](https://www.gotquestions.org/dispensationalism.html) | All covenants fall under the Covenant of Grace. They are the same in substance but different in administration.[[7]](https://westminsterstandards.org/westminster-confession-of-faith/) |

### Distinctions

| Framework | Dispensationalism | Covenantalism |
|---|---|---|
| **Interpretation** | Passages are interpreted literally, unless text explicitly instructs otherwise. | Passages interpreted based on the overarching covenant | 
| **Christological Viewpoint** | Christotelic - “even though God (“Yahweh”) is noted throughout the Old Testament, and even though Jesus is God, many of the Old Testament passages often referred to as Messianic do not directly point to Jesus.” [[8]](https://www.gotquestions.org/Christocentric-Christotelic.html) |  Christocentric - “seeing the Christ or Messiah on “every page” or at least regularly throughout the Old Testament writings” [[8]](https://www.gotquestions.org/Christocentric-Christotelic.html) |
| **Church and Israel** | Israel and the Church are distinct [[5]](https://www.gotquestions.org/dispensationalism.html) |  Church as part of Israel (God’s chosen people) |
| **Fulfillment of Promises** | Promises to Israel will be fulfilled in Millennium [[5]](https://www.gotquestions.org/dispensationalism.html) | Promises to Israel are fulfilled in Christ [[6]](https://www.gotquestions.org/covenant-theology.html) or will be fulfilled in New Creation [[9]](https://www.ligonier.org/learn/devotionals/new-heavens-and-earth) |

## **Key Texts**
### Foreword
Dispensational framework argues for a literal interpretation of prophecies - for a literal nation of Israel. I will start by giving examples of prophecies that it argues have to be interpreted literally, and will be be fulfilled literally.

Dispensationalism doesn’t need a reason to interpret these prophecies literally, but Covenantalism does need a reason and merit for interpreting them more figuratively.

So I will follow up with texts that present merit to Covenantal views of:
* Prophecies’ fulfilment by Christ/ in Christ
* Church being grafted to Israel, making a single “people of God”, described as “children of God”, “Abraham’s descendants”, and “heirs according to the promise”
* Heavenly nature of land promises
* Real future for Israel

### Dispensationalism - Prophecies
#### Ruler related promises, David’s Throne
**Luke 1:32-33** (based on **2 Samuel 7:12-16**):

“He will be great and will be called the Son of the Most High; and the Lord God will give Him the <u>throne of His father David</u>; and He will reign over the <u>house of Jacob</u> forever, and His kingdom will have no end.”

*- Dispensational view puts emphasis on “David’s throne” - which is earthly, but after millennium will <u>transition</u> into heavenly, endless reign.*

#### Peace and Harmony related promises 
**Isaiah 11:10**:

“[4a] But with righteousness He will judge the <u>poor</u>, And decide with fairness for the humble <u>of the earth</u>… [6a] And the wolf will dwell with the lamb… [9b] For <u>the earth</u> will be full of the knowledge of the Lord… [10] Then on that day The nations will resort to the <u>root of Jesse</u>, Who will stand as a signal flag for the peoples; And His resting place will be glorious.”

**Isaiah 65:17–25**:
“[17] For behold, I create new heavens and a new earth; And the former things will not be remembered or come to mind…[20b] For the youth will <u>die</u> at the age of a hundred… [25b] lion will eat straw like the ox…”

*- Presence of death and poverty suggests an earthly nature*

#### Land related promises
Genesis 15:18-21:
On that day the Lord made a covenant with <u>Abram</u>, saying, “To your descendants I have given this land, <u>From the river of Egypt as far as the great river, the river Euphrates</u>: the <u>land of the Kenite, the Kenizzite, the Kadmonite, the Hittite, the Perizzite, the Rephaim, the Amorite, the Canaanite, the Girgashite, and the Jebusite</u>.”
See also **Zechariah 14:9-20, Ezekiel 20**, etc. 

*- Mentions of a specific places suggests a literal, physical nature of the land. Israel has never fully possessed this land, so its expected to happen during the millennium.*

After Land Promises in **Ezekiel 20, in v49**:
“Then I said, “Oh, Lord God! They are saying of me, ‘Is he not just speaking in riddles?’””

*- Verse implies that author intended these to be taken seriously, **more than “just riddles”**.*

**Ezekiel 36:19-24**:
“Therefore I poured out My wrath on them [house of Israel] for the blood which they had shed on the land, because they had defiled it with their idols. I also scattered them among the nations, and they were dispersed throughout the lands. According to their ways and their deeds I judged them… For I will take you [Israel] from the nations, and gather you from all the lands; and I will bring you into your own land.”

*- Scattering was literal, which implies that gathering will be literal as well. (Although, v25-27 contain spiritual promises)*

### Dispensationalism - Prophecies - Resolution

Dispensationalism states that all these promises are to be fulfilled in Millennium Reign of Christ in **Revelations 20:4-6**:
“Then I saw thrones, and they sat on them, and judgment was given to them. And I saw the souls of those who had been beheaded because of their testimony of Jesus and because of the word of God, and those who had not worshiped the beast or his image, and had not received the mark on their foreheads and on their hands; and they came to life and reigned with Christ for a thousand years. The rest of the dead did not come to life until the thousand years were completed. This is the first resurrection. Blessed and holy is the one who has a part in the first resurrection; over these the second death has no power, but they will be priests of God and of Christ, and will reign with Him for a thousand years.”

*- Even though Israel is not explicitly mentioned, the connection of Reign of Christ and the promise to Israel of David’s seed reigning leads dispensationalists to conclude that all these promises will be fulfilled in this Millenium period.* 

*Literal interpretation of these prophecies lead Dispensationalists to conclude that - post-rapture, during Millenium, David’s seed will rule on David’s earthly throne, over specific lands, and His rule will bring peace and prosperity to national Israel.* 

### Covenantalism - To whom do the promises apply. 
**2 Corinthians 1:20**:
“For as many as the <u>promises of God are, in Him [Christ] they are yes</u>; therefore through Him also is our Amen to the glory of God through us.”
*- Promises of God are fulfilled in Christ”

**Romans 9:6-8**:
“But it is not as though the word of God has failed. For <u>they are not all Israel who are descended from Israel</u>; nor are they all children because they are Abraham’s descendants, but: “through Isaac your descendants shall be named.” That is, <u>it is not the children of the flesh who are children of God, but the children of the promise are regarded as descendants.</u>”
*- Being born Jewish doesn’t necessarily make one “a child of promise” [[10]](https://www.desiringgod.org/messages/who-is-the-true-israel) 

**Romans 11:17**:
“But if some of the branches were broken off, and you [gentiles], being a wild olive, were <u>grafted in</u> among them [Israel] and became partaker with them of the rich root of the olive tree [covenant promises]”

*- Gentiles become partakers of covenant promises made to ethnic Israel through being “grafted into the tree [Israel]”*
**Galatians 3:29**:
“There is <u>neither Jew nor Greek</u>, there is neither slave nor free, there is neither male nor female; for you are all one in Christ Jesus. And <u>if you belong to Christ, then you are Abraham’s descendants, heirs according to promise.</u>”

*- “Belonging to Christ” is the criteria to being Abraham’s descendant and an heir to the promise - not ethnicity*

(see also Romans 9-11, Ephesians 2:11-12)

### Covenantalism - Regarding land-related promises

#### Kingdom not of this earth
**John 18:36**:
“Jesus answered, “<u>My kingdom is not of this world.</u> If My kingdom were of this world, My servants would be fighting so that I would not be handed over to the Jews; but as it is, My <u>kingdom is not of this realm.</u>””

*- Jesus says that His kingdom is heavenly, not earthly. “that the kingdom of Christ is not in this world; for we know that it has its seat in our hearts” (see Luke 17:21)* [[11]](https://biblehub.com/commentaries/calvin/john/18.htm)

**Hebrews 11:10, 16**:
“[10] for he [Abraham] was looking for the city which has foundations, whose architect and builder is God.”
“[16] But as it is, they desire a better <u>country, that is, a heavenly one</u>. Therefore God is not ashamed to be called their God; for He has prepared a <u>city</u> for them.”

*- Abraham and the patriarchs are said to be looking for a "better country," a heavenly one, not just the land of Canaan, indicating that the ultimate fulfillment transcends geography.*

### Covenantalism - Future for Israel
**Ezekiel 36:24-28**:

“For I will take you from the nations, and gather you from all the lands; and I will bring you into <u>your own land.</u> Then I will sprinkle clean water on you, and you will be clean; I will cleanse you from all your filthiness and from all your idols. Moreover, I will give you a <u>new heart and put a new spirit</u> within you; and I will remove the heart of stone from your flesh and give you a heart of flesh. And <u>I will put My Spirit within you</u> and bring it about that you walk in My statutes, and are careful and follow My ordinances. And <u>you will live in the land</u> that I gave to your forefathers; so you will be My people, and I will be your God.”

*- Passage implies that Israel land promises may have something to do with their spiritual conversion.*

Spiritual conversion promise reiterated in **Romans 11:26**:
“and so all Israel will be saved; just as it is written,”

*- “someday the nation as a whole will be converted to Christ” (not necessarily every individual; see 1 Kings 12:1; 2 Chronicles 12:1)* [[12]](https://www.desiringgod.org/articles/five-reasons-i-believe-romans-11-26-means-a-future-conversion-for-israel) [[13]](https://www.ligonier.org/learn/sermons/israels-rejection-not-final-part-2)

## Comparing language used for Israel and the Church

### People of God

Both frameworks affirm that Church and Israel are “people of God”.

**Dispensational** - they are two **distinct** “people of God”

**Covenantal** - “people of God” is **one people**.

Israel described as “people of God” - **Exodus 6:7**:
"Then I will take you for <u>My people</u>, and I will be your God; and you shall know that I am the Lord your God, who brought you out from under the burdens of the Egyptians."
(Also Deut 26:18)

Church described as “people of God” - **1 Peter 2:9-10**:
“But you are a chosen race, a royal priesthood, a holy nation, a people for God’s own possession, so that you may proclaim the excellencies of Him who has called you out of darkness into His marvelous light; for you once were not a people, but now you are the <u>people of God</u>; you had not received mercy, but now you have received mercy."
(Also 2 Cor 6:16)

### The Bride of Christ

Both Israel and the Church are called the bride of Christ.

**Israel** as God’s bride - **Jeremiah 31:32**:
“not like the covenant that I made with their fathers on the day when I took them by the hand to bring them out of the land of Egypt, my covenant that they broke, though <u>I was their husband</u>, declares the Lord.”
(see also Isaiah 54:5, Hosea 2:19-20, 3:1, etc.)

**Church** as Christ’s bride - **2 Corinthians 11:2**:
"For I am jealous for you with a godly jealousy; for I <u>betrothed</u> you to one <u>husband</u>, to present you as a pure virgin to <u>Christ</u>."
(see also Revelation 19:7-8, Ephesians 5:25-27, Revelation 21:9, John 3:29, etc.)

The Bible is clear that a husband is to have only one wife. Earthly marriage is a reflection of Christ’s relationship to His church. Moreover, Revelation 19:7-9; 21:2,9 refers to a single bride, wife of the Lamb. If church and Israel are two distinct “people of God”, it could be said that God has two brides. **If the church is part of Israel (aka “People of God”) - God has only one bride**. 

## Outro

“Covenant Theology and Dispensationalism have many differences, and sometimes lead to opposite conclusions regarding certain secondary doctrines, but both adhere to the essentials of the Christian faith: salvation is by grace alone, through faith alone in Christ alone, and to God alone be the glory!” [[6]](https://www.gotquestions.org/covenant-theology.html)

## Citations

*Quotes are from 2024 versions of the websites. If needed, use WaybackMachine to verify. 

[1] Wordnik - “Framework”. (https://www.wordnik.com/words/framework) 

Common definition of the word, used to help understand the topic at hand.


[2] Wordnik - “Dispense”. (https://www.wordnik.com/words/dispense) 

Common definition of the word at the root of “Dispensationalism” and the defining characteristic and distinguishing trait of the framework. 


[3] Wordnik - “Covenant”. (https://www.wordnik.com/words/covenant) 

Common definition of the word at the root of “Covenantalism” and the defining characteristic and distinguishing trait of the framework. 


[4] Clarence B. Bass - “Backgrounds to Dispensationalism”, p.19. (https://archive.org/details/backgroundstodis0000bass/page/18/mode/2up) 

The claim that “God works under different principles” at different periods of time/ dispensations. FYI - I did not read the book completely, I just found it quoted on Wikipedia, and then found the quote itself. 


[5] GotQuestions - “What is dispensationalism and is it biblical?” (https://www.gotquestions.org/dispensationalism.html) 

Seems to be an unbiased source, with articles for both Dispensationalism and Covenantalism. This specific article is quoted 4 times in my work, for the purpose of describing a commonly agreed-upon definition of Dispensationalism. During my research I found it common to define Dispespensationalism as whatever is convenient for the argument at-hand. Examples of this can be clearly seen when scholars refer to early church mentions of premelenialism, and using those as historic examples of Dispensationalism.

I think it is beneficial to keep the definition pure and defined, rather than blurring the definition of it to the point where it means whatever you want it to mean. I find it very helpful, and welcome addition of adjectives in order to define a deviation from the common definition. For example - John MacArthur calls himself a “Leaky Dispensationalist”


[6] GotQuestions - “What is Covenant Theology?” (https://www.gotquestions.org/covenant-theology.html) 

Similar to their article on Dispensationism, although it does not suffer from the same blurring of the definition. For example, there are “New Covenent Theology”, “Historical Premillennialism”, “Progressive Covenentalism”, and “1689 Federalism” - all are different variants of Covenentalism, and are well defined on their own.


[7] Westminster Confession of Faith - “Chapter VII. Of God's Covenant with Man” (https://westminsterstandards.org/westminster-confession-of-faith/) 

“Man, by his fall, having made himself incapable of life by that covenant [covenant of works], the Lord was pleased to make a second, (Gal. 3:21, Rom. 8:3, Rom. 3:20–21, Gen. 3:15, Isa. 42:6) commonly called the covenant of grace; wherein He freely offereth unto sinners life and salvation by Jesus Christ; requiring of them faith in Him, that they may be saved…”


[8] GotQuestions - “What is the difference between a Christocentric and a Christotelic hermeneutic?” (https://www.gotquestions.org/Christocentric-Christotelic.html) 

Used to draw distinction between Dispensational and Covenental Christological viewpoints.


[9] Ligonier - “The New Heavens and Earth [devotional]”  (https://learn.ligonier.org/devotionals/new-heavens-and-earth) 

Quotes John Calvin in agreement that some promises will be fulfilled at the last resurrection, or at new heaven and earth.


[10] Desiring God - John Piper - “Who is the True Israel” (https://www.desiringgod.org/messages/who-is-the-true-israel) 

John Piper’s exposition of Romans 9:6-8, defines “True Israel” by Covenental interpretation of the passage. 


[11] John Calvin - John 18 Commentary (https://biblehub.com/commentaries/calvin/john/18.htm) 

In addition to “New Heaven and Earth” fulfillment of promises described in quote 9,this is an example of more figurative fulfillment of promises. Example of how promises of things such as Kingdoms could be fulfilled in hearts of believers.


[12] Desiring God - John Piper - “Five Reasons I Believe Romans 11:26 Means a Future Conversion for Israel
” (https://www.desiringgod.org/articles/five-reasons-i-believe-romans-11-26-means-a-future-conversion-for-israel) 

[13] Legonier - R. C. Sproul - “Israel's Rejection Not Final (Part 2)” (https://learn.ligonier.org/sermons/israels-rejection-not-final-part-2) 

Common strawman of Covenantalism is that they rule out any real future for Israel [(example)](https://www.youtube.com/watch?v=rtue2oFiIMQ). But that is not Covenantalism - thats Supersessionism. These articles by Desiring God (John Piper) and Ligonier (R. C. Sproul) (see timestamps 38:30, 40:15) are an example of real future for national Israel within Covenantal framework. 