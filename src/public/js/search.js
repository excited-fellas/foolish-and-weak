/** The following script relies on "data" object existing. That object must have
 * all the output of a particular 11ty collection's JSON to be processed */

const showMoreThreshold = 5;
const titleSearchInputElement = $("#title-search");
var filterCollapsed = false;

const emojisForFilters = {
	//"equipment": "🏀",
	//"age": "👶",
	//"preparation": "📝",
	"categories": ""
}

// Who needs jQuery?
function $(q) {
	let queryReturn = document.querySelectorAll(q);
	return queryReturn.length > 1 ? queryReturn : queryReturn[0];
}

function capFirst(text){
	return text.charAt(0).toUpperCase() + text.slice(1);
}

/** `filtersObject` refers to the object with all the filter categories
 * 	`filterObject` refers to an object of a single filter category
 * 	Sorry for the similar variable names, I'll consider your recommendations */

(function () {
	const render = {
		createEl: (elString, className = null, textContent = null) => {
			const el = document.createElement(elString);
			if(className) el.className = className;
			if(textContent) el.textContent = textContent;
			return el;
		},
		resultEl: function(game){
			const gameElement = render.createEl("a", "result-item");
			gameElement.setAttribute("href", game.url);
			gameElement.setAttribute("target", "_blank");

			const firstColumn = render.createEl('div');
			const secondColumn = render.createEl('div', 'detail-list');

			const titleElement = render.createEl("p", "game-title");
			titleElement.textContent = game.title;

			const categoriesElement = render.createEl('div', 'tags-list');
			game.categories.split(",").forEach(categoryText => 
				categoriesElement.appendChild(
					render.createEl('span', 'tags-entry', "#" + categoryText)
				)
			);

			firstColumn.appendChild(titleElement);
			firstColumn.appendChild(categoriesElement);

			// TODO: Refactor this into something that is readable
			["equipment", "age", "preparation"]
				.filter(detailName => game[detailName])
				.forEach(detailName => {
					const detailsEntryEl = render.createEl("p", "details-entry");

					detailsEntryEl.appendChild(
						render.createEl(
							"span",
							"details-icon details-icon-" + detailName,
							emojisForFilters[detailName] + ":"
						)
					);
					detailsEntryEl.appendChild(
						// TODO: Maybe some entries require tooltips with question marks (ex. preparation) ? 
						render.createEl(
							"span",
							"details-data",
							// adding " " after a comma if there is a comma
							game[detailName].split(",").join(", "))
					);

					secondColumn.appendChild(detailsEntryEl);
				});

			gameElement.appendChild(firstColumn);
			gameElement.appendChild(secondColumn);

			return gameElement;
		},
		checkBoxFilterItem: function (filterItemObj) {
			const filterItemDiv = render.createEl("div", "filter-item");
			const wrapperLabel = render.createEl("label");
			
			const inputEl = render.createEl("input");
			inputEl.type = "checkbox";
			inputEl.className = "checkbox-input";
			inputEl.checked = filterItemObj.value;
			inputEl.name = filterItemObj.name;
			inputEl.addEventListener("input", function(e){
				filterItemObj.value = e.target.checked;
				filtersUpdated();
			})

			wrapperLabel.appendChild(inputEl);
			wrapperLabel.appendChild(render.createEl("span", "fake-checkbox"));
			wrapperLabel.appendChild(render.createEl("span", "filter-item-text", capFirst(filterItemObj.name)));
			filterItemDiv.appendChild(wrapperLabel);

			return filterItemDiv;
		},
		filterGroupHeader: function(filterObject) {
			// TODO: implement collapsing?

			const headerElement = render.createEl("div", "filter-header");

			const headerTitle = render.createEl("span", "filter-header-title");
			headerTitle.textContent = emojisForFilters[filterObject.attribute] + " " + filterObject.title;
			headerTitle.addEventListener("click", this.toggleHeaderClosed);

			const selectAllButton = render.createEl("button");
			selectAllButton.textContent = "Select All";
			selectAllButton.addEventListener("click", function(e){
				filterObject.values.forEach(v => v.value = true);
				filtersUpdated();
				renderFilters();
			});

			const clearSelectionButton = render.createEl("button");
			clearSelectionButton.textContent = "Clear";
			clearSelectionButton.addEventListener("click", function(e){
				filterObject.values.forEach(v => v.value = false);
				filtersUpdated();
				renderFilters();
			});

			headerElement.appendChild(headerTitle);
			headerElement.appendChild(selectAllButton);
			headerElement.appendChild(clearSelectionButton);

			return headerElement;
		},
		toggleHeaderClosed: function(event) {
			const filterGroup = event.target.parentElement.parentElement;
			filterGroup.classList.toggle("closed");
		},
		// filtersObject reference is here for function binding.
		showMoreButton: function(filterObject){
			const showMoreElement = render.createEl("span", "show-more-text", "Show more...");
			showMoreElement.addEventListener("click", () => {
				filterObject.showMore = false;
				renderFilters();
			});
			return showMoreElement;
		},
		filterGroup: function (filterObject) {
			const filterGroupElement = render.createEl("div", "filter-group");
			const filterGroupHeader = render.filterGroupHeader(filterObject);
			const filterItemsWrapper = render.createEl("div", "filter-item-wrapper");

			let showMoreRendered = false;
			filterObject.values.forEach( (filterItem, ind) => {
				if (filterObject.showMore && ind >= 5) {
					if (!showMoreRendered) {
						filterItemsWrapper.appendChild(render.showMoreButton(filterObject));
						showMoreRendered = true;
					}
					return void 0;
				}

				filterItemsWrapper.appendChild(
					render.checkBoxFilterItem(filterItem)
				);
			})

			filterGroupElement.appendChild(filterGroupHeader);
			filterGroupElement.appendChild(filterItemsWrapper);

			return filterGroupElement;
		}
	}

	// Creating the Filters array.
	const filterableAttributes = ["categories"]
	const filters = [];
	let filteredData = data;
	filterableAttributes.forEach(attribute => {
		const title = capFirst(attribute);
		const values = data
			.map(el => el[attribute].split(","))
			.filter(el => el[0].length > 0)
			.reduce((acc, curr) => {
				return acc.concat(curr);
			}, [])
			// unique-ing the strings by previous index requires sorting
			.sort()
			.filter((value, index, array) => {
				return (index === 0) || (value !== array[index - 1]);
			})
			.map(name => ({ name, value: false }));
		
		const showMore = values.length > showMoreThreshold;
		filters.push({ attribute, title, values, showMore })
	});

	// toggle filter collapse
	$("#filter-collapse").addEventListener("click", function (event){
		filterCollapsed = !filterCollapsed;
		const filtersContainerElement = $(".filters-container");
		if(filterCollapsed) {
			console.log(event.target);
			event.target.innerHTML = "[expand]";
			filtersContainerElement.style.maxHeight = "0px";
			filtersContainerElement.style.overflow = "hidden";
		} else {
			console.log(event.target);
			event.target.innerHTML = "[collapse]";
			filtersContainerElement.style.maxHeight = "";
			filtersContainerElement.style.overflow = "";
		}
	})

	function renderResults(data = data, container = $("#results-container")){
		const childrenNodes = [];
		data.forEach(g => {
			childrenNodes.push( render.resultEl(g) );
		});
		container.replaceChildren(...childrenNodes);
	}

	function renderFilters(filtersObject = filters, container = $(".filters-container")) {
		const childrenNodes = [];
		filtersObject.forEach(filter => {
			childrenNodes.push( render.filterGroup(filter) );
		});
		container.replaceChildren(...childrenNodes);
	}

	renderFilters(filters, $(".filters-container"));
	renderResults(filteredData, $("#results-container"));

	// Triggers the attribute filtering (I think title text filtering will be done separately)
	function filtersUpdated(){
		// this will change to something meaningful later
		let filteringValues = filters.map(filterGroup => ({
			attribute: filterGroup.attribute,
			values: filterGroup.values.filter(el => {
				return el.value === true;
			}).map(el => el.name)
		})).filter( filterGroup => filterGroup.values.length > 0);

		filteredData = data.filter(gameObject => {
			// returns a boolean for each filterGroup
			return filteringValues.map(filterGroup => {
				return filterGroup.values.every(v =>
					gameObject[filterGroup.attribute].split(",").includes(v)
				);
			// if all of them are true, filtering goes through
			}).indexOf(false) === -1;
		});

		titleSearchFilter(titleSearchInputElement.value);
	}

	function titleSearchFilter(text){
		const regexp = new RegExp(text, "i");
		const textFilteredResults = filteredData.filter(
			d => {
				return regexp.test(d.title);
			}
		)
		renderResults(textFilteredResults, $("#results-container"));
	}

	titleSearchInputElement.addEventListener("input", e => {
		titleSearchFilter(e.target.value);
	});

	filtersUpdated();
})();