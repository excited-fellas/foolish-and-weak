(function(){
	let animationDuration = 250,
		firstFocusableElement,
		lastFocusableElement,
		slideOutContainer,
		mainElement,
		openNavButton,
		closeNavButton;

	// who needs jQuery
	function $(q){
		let queryReturn = document.querySelectorAll(q);
		return queryReturn.length > 1 ? queryReturn : queryReturn[0];
	}

	function initNavigation(){
		mainElement = $("main");
		openNavButton = $("#hamburger-menu");
		closeNavButton = $("#close-btn");
		slideOutContainer = $("#slide-nav");

		// focus trapping
		const focusableElementsQuery = '#slide-nav button, #slide-nav [href], #slide-nav\
			input, #slide-nav select, #slide-nav textarea, #slide-nav\
			[tabindex]:not([tabindex="-1"])';
		const focusableElements = $(focusableElementsQuery);
		firstFocusableElement = focusableElements[0];
		lastFocusableElement = focusableElements[focusableElements.length - 1];

		// listeners
		addEventListeners();
		trapFocus();
	}

	function addEventListeners() {
		openNavButton.addEventListener("click", openNavigation);
		closeNavButton.addEventListener("click", closeNavigation);
		slideOutContainer.addEventListener("keyup", closeNavigation);
	}

	function trapFocus(){
		firstFocusableElement.addEventListener("keydown", e => {
			if(e.key === "Tab" && e.shiftKey){
				e.preventDefault();
				lastFocusableElement.focus();
			}
		});
		lastFocusableElement.addEventListener("keydown", e => {
			if(e.key === "Tab" && !e.shiftKey){
				e.preventDefault();
				firstFocusableElement.focus();
			}
		});
	}

	function openNavigation(){
		slideOutContainer.classList.add("open");
		slideOutContainer.classList.add("visible");
		closeNavButton.setAttribute("aria-expanded", "true");
		closeNavButton.focus();
		openNavButton.setAttribute("aria-expanded", "true");
		mainElement.setAttribute("aria-hidden", "true");
	}

	function closeNavigation(e){
		if(e.type === "keyup" && e.key !== "Escape") return;

		slideOutContainer.classList.remove("open");
		mainElement.setAttribute("aria-hidden", "false");
		openNavButton.setAttribute("aria-expanded", "false");
		openNavButton.focus();
		closeNavButton.setAttribute("aria-expanded", "false");

		setTimeout(function(){
			slideOutContainer.classList.remove("visible");
		}, animationDuration);
	}
	
	initNavigation();
})();