module.exports = {
	title: "Foolish & Weak",
	url: "https://foolishandweak.org/",
	language: "en",
	description: "Library of my sermons and articles in an easy to find and convenient place.",
	author: {
		name: "Excited Fellas",
		url: "https://excitedfellas.com/"
	}
}
